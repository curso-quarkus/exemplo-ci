package curso.projeto;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/categoria")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CategoriaResource {

    @Inject
    private CategoriaRepository categoriaRepository;

    @GET
    public List<Categoria> listarCategorias(){
        return categoriaRepository.listAll();
    }

    @POST
    @Transactional
    public Categoria salvarCategoria(@Valid Categoria categoria){
        categoria.persist();
        return categoria;
    }

}